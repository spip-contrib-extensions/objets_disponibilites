<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/objets_disponibilites.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'champ_disponibilites_objets' => 'Activer l’ajout des disponibilités sur les contenus :',

	// O
	'objets_disponibilites_titre' => 'Disponibilités objets',

	// T
	'titre_page_configurer_objets_disponibilites' => 'Paramètres Disponibilités objets'
);
